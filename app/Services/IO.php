<?php

namespace App\Services;

/**
 * Class IO
 * @package App\Services
 */
class IO
{
    /**
     * @var Blackfire
     */
    protected $blackfire;

    /**
     * @param Blackfire $blackfire
     */
    public function __construct(Blackfire $blackfire)
    {
        $this->blackfire = $blackfire;
    }

    /**
     * @return string
     */
    public function test()
    {
        $this->blackfire->downloadBlackFire();

        return 'OK IO';
    }
}