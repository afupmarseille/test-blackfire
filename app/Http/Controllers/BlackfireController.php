<?php

namespace app\Http\Controllers;

use App\Services\Cpu;
use App\Services\IO;
use App\Services\Memory;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;

/**
 * Class BlackfireController
 * @package app\Http\Controllers
 */
class BlackfireController extends Controller
{
    /**
     * Test CPU
     *
     * @param Cpu $cpu
     *
     * @return Response
     */
    public function cpu(Cpu $cpu)
    {
        return view('blackfire.cpu', ['cpu' => $cpu]);
    }

    /**
     * Test Memory
     *
     * @param Memory $memory
     *
     * @return Response
     */
    public function memory(Memory $memory)
    {
        return view('blackfire.memory', ['memory' => $memory]);
    }

    /**
     * Test Memory
     *
     * @param IO $io
     *
     * @return Response
     */
    public function io(IO $io)
    {
        return view('blackfire.io', ['io' => $io]);
    }

    /**
     * Test All
     *
     * @param Cpu    $cpu
     * @param Memory $memory
     * @param IO     $io
     *
     * @return Response
     */
    public function index(Cpu $cpu, Memory $memory, IO $io)
    {
        return view('blackfire.index', [
            'cpu' => $cpu,
            'memory' => $memory,
            'io' => $io
        ]);
    }
}