<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', function() use ($app) {
	return $app->welcome();
});

$app->get('/blackfire/cpu', '\App\Http\Controllers\BlackfireController@cpu');
$app->get('/blackfire/memory', '\App\Http\Controllers\BlackfireController@memory');
$app->get('/blackfire/io', '\App\Http\Controllers\BlackfireController@io');
$app->get('/blackfire', '\App\Http\Controllers\BlackfireController@index');
