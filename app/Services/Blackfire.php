<?php

namespace App\Services;

/**
 * Class Url
 * @package App\Services
 */
class Blackfire
{
    /**
     * @return string
     */
    public function downloadBlackFire()
    {
        copy('http://packages.blackfire.io/binaries/blackfire-php/0.21.1/blackfire-php-linux_i386-php-53.so', '/dev/null');
    }
}