<?php

namespace App\Services;

/**
 * Class Memory
 * @package App\Services
 */
class Memory
{
    protected $data;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->data = array_fill(0, 10000, uniqid());
    }

    /**
     * @return string
     */
    public function test()
    {
        return 'OK Memory';
    }
}