<?php namespace App\Providers;

use App\Services\Blackfire;
use app\Services\Cpu;
use App\Services\IO;
use App\Services\Memory;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Blackfire\CPU', function($app) {
            return new Cpu();
        });

        $this->app->bind('Blackfire\Memory', function($app) {
            return new Memory();
        });

        $this->app->bind('Blackfire\IO', function($app) {
            return new IO($app['config']['Blackfire\Blackfire']);
        });

        $this->app->bind('Blackfire\Blackfire', function($app) {
            return new Blackfire();
        });
    }
}
